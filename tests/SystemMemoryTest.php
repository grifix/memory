<?php
declare(strict_types=1);

namespace Grifix\Memory\Tests;

use Grifix\Memory\MemoryInterface;
use Grifix\Memory\SystemMemory;
use PHPUnit\Framework\TestCase;

final class SystemMemoryTest extends TestCase
{
    public function testItCreates(): void
    {
        $memory = new SystemMemory();
        self::assertInstanceOf(MemoryInterface::class, $memory);
    }
}
