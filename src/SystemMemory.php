<?php
declare(strict_types=1);

namespace Grifix\Memory;

final class SystemMemory implements MemoryInterface
{
    public function getUsage() : int
    {
        return memory_get_usage(true);
    }
}
