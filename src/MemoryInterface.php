<?php
declare(strict_types=1);

namespace Grifix\Memory;

interface MemoryInterface
{
    public const KB = 1024;
    public const MB = 1024 * self::KB;
    public const GB = 1024 * self::MB;
    public const TB = 1024 * self::GB;

    public function getUsage(): int;
}
